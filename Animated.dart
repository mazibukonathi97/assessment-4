import 'package:flutter/material.dart';

void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  final appTitle = 'HOME PAGE';
 
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
          body: MyHomePage(title: appTitle)),
    );
  }
}
 
class MyHomePage extends StatelessWidget {
  final String title;
 
  MyHomePage({Key? key, required this.title}) : super(key: key);
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Center(child: Text('My Page!')),
   

    AnimatedCrossFade(
      firstcurve: Curves.fastOutSlowIn,
      crossFadeState: !expanded ? CrossFadeState.showFirst : CrossFadeState.showSecond,
      duration: transitionDuration,
      firstChild: Container(),
      secondChild: _logoRemainder(),
      alignment: Alignment.centerLeft,
      suzeCurve: Curves.easeInOut,
    ),
      ),
  }
}